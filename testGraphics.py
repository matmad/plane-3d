
import graphics3D as graphics2D
from graphics3D import point, Layer
from vector import Vector3, Vector2
from math import pi


resolution = Vector2(600, 480)
graphics2D.setResolution(resolution)
# myGraphics.setWindowResizable()
graphics2D.setTitle("Test Window")
graphics2D.setBackgroundColor((255,255,255,255))

background = graphics2D.newLayer()
graphics2D.fill(background)
background2:Layer = graphics2D.newLayer()
graphics2D.fill(background2,(100,100,100,0))
graphics2D.drawn_rectangle(background2,(4,4),(200,200),(100,100,100,0))

testVectorielLayer = graphics2D.newVectorielLayer()
mousePointerA = graphics2D.VecCercle((10,10),20,(150,255,255,255),None,2)
mousePointerB = graphics2D.VecCercle((10,10),10,None,(150,255,255,255),0)
testVectorielLayer.objects_add(mousePointerA)
testVectorielLayer.objects_add(mousePointerB)
# myGraphics.render()

noir = (0,0,0,255)
graphics2D.drawn_line(background,(10,10),(50,50),noir,4)
graphics2D.drawn_cercle(background,(50,50),20,noir)
graphics2D.drawn_cercle_empty(background,(100,100),20,noir,5)
graphics2D.drawn_text(background,"Hello World",(5,100),noir,20)
graphics2D.drawn_textCenter(background,"Hello World",(5,130),noir,20)
graphics2D.drawn_textComplexe(background,"Hello World",(5,160),(0.25,0.25),noir,(100,100,100,255),20)
# graphics2D.setLevelAlpha(background,128)
# background.changeAlpha(128)

Points={
    # Body Right Top
    "BRT1":point(Vector3(0.651,-0.973,3.202)),
    "BRT2":point(Vector3(1,0,1.202)),
    "BRT3":point(Vector3(1,0,-1.769)),
    "BRT4":point(Vector3(0.364,0,-5.886)),
    "BRT5":point(Vector3(0.365,-0.010,-6.690)),
    # Body Right Bottom
    "BRB1":point(Vector3(0.651,-1.881,3.202,)),
    "BRB2":point(Vector3(1,-2.100,1.202)),
    "BRB3":point(Vector3(1,-2.100,-1.769)),
    "BRB4":point(Vector3(0.364,-0.884,-5.886)),
    "BRB5":point(Vector3(0.365,-0.300,-6.690)),
    
    # Body Left Top
    "BLT1":point(Vector3(-0.651,-0.973,3.202)),
    "BLT2":point(Vector3(-1,0,1.202)),
    "BLT3":point(Vector3(-1,0,-1.769)),
    "BLT4":point(Vector3(-0.364,0,-5.886)),
    "BLT5":point(Vector3(-0.365,-0.010,-6.690)),
    # Body Left Bottom
    "BLB1":point(Vector3(-0.651,-1.881,3.202,)),
    "BLB2":point(Vector3(-1,-2.100,1.202)),
    "BLB3":point(Vector3(-1,-2.100,-1.769)),
    "BLB4":point(Vector3(-0.364,-0.884,-5.886)),
    "BLB5":point(Vector3(-0.365,-0.300,-6.690)),

    # Ailes Left Top
    "ALT1":point(Vector3(-6.976,0.091,0.204)),
    "ALT2":point(Vector3(-5.333,0.091,0.764)),
    "ALT3":point(Vector3(-2.667,0.091,1)),
    "ALT4":point(Vector3(0,0.091,1)),
    "ALT5":point(Vector3(-6.976,0.091,-1.562)),
    "ALT6":point(Vector3(-5.333,0.091,-1.562)),
    "ALT7":point(Vector3(-2.667,0.091,-1.562)),
    "ALT8":point(Vector3(0,0.091,-1.562)),
    # Ailes Right Top
    "ART1":point(Vector3(6.976,0.091,0.204)),
    "ART2":point(Vector3(5.333,0.091,0.764)),
    "ART3":point(Vector3(2.667,0.091,1)),
    "ART4":point(Vector3(0,0.091,1)),
    "ART5":point(Vector3(6.976,0.091,-1.562)),
    "ART6":point(Vector3(5.333,0.091,-1.562)),
    "ART7":point(Vector3(2.667,0.091,-1.562)),
    "ART8":point(Vector3(0,0.091,-1.562)),
    # Ailes Left Bottom
    "ALB1":point(Vector3(-6.976,-0.091,0.204)),
    "ALB2":point(Vector3(-5.333,-0.091,0.764)),
    "ALB3":point(Vector3(-2.667,-0.091,1)),
    "ALB4":point(Vector3(0     ,-0.091,1)),
    "ALB5":point(Vector3(-6.976,-0.091,-1.562)),
    "ALB6":point(Vector3(-5.333,-0.091,-1.562)),
    "ALB7":point(Vector3(-2.667,-0.091,-1.562)),
    "ALB8":point(Vector3(0     ,-0.091,-1.562)),
    # Ailes Right Bottom
    "ARB1":point(Vector3(6.976,-0.091,0.204)),
    "ARB2":point(Vector3(5.333,-0.091,0.764)),
    "ARB3":point(Vector3(2.667,-0.091,1)),
    "ARB4":point(Vector3(0    ,-0.091,1)),
    "ARB5":point(Vector3(6.976,-0.091,-1.562)),
    "ARB6":point(Vector3(5.333,-0.091,-1.562)),
    "ARB7":point(Vector3(2.667,-0.091,-1.562)),
    "ARB8":point(Vector3(0    ,-0.091,-1.562)),
    
    # Aleron Left Top
    "ELT1":point(Vector3(-0  ,0.121,-5.411)),
    "ELT2":point(Vector3(-1  ,0.121,-5.411)),
    "ELT3":point(Vector3(-2.9,0.121,-5.411)),
    "ELT4":point(Vector3(-3.5,0.121,-6.411)),

    "ELT5":point(Vector3(-0  ,0.121,-6.836)),
    "ELT6":point(Vector3(-1  ,0.121,-6.836)),
    "ELT7":point(Vector3(-2.9,0.121,-6.836)),
    "ELT8":point(Vector3(-3.5,0.121,-6.836)),
    # Aleron Right Top
    "ERT1":point(Vector3(0  ,0.121,-5.411)),
    "ERT2":point(Vector3(1  ,0.121,-5.411)),
    "ERT3":point(Vector3(2.9,0.121,-5.411)),
    "ERT4":point(Vector3(3.5,0.121,-6.411)),

    "ERT5":point(Vector3(0  ,0.121,-6.836)),
    "ERT6":point(Vector3(1  ,0.121,-6.836)),
    "ERT7":point(Vector3(2.9,0.121,-6.836)),
    "ERT8":point(Vector3(3.5,0.121,-6.836)),
    
    # Aleron Debout Left
    "DL1":point(Vector3(-0.184,-0.026,-4.997)),
    "DL2":point(Vector3(-0.184, 1.626,-6.553)),
    "DL3":point(Vector3(-0.184, 1.751,-7.514)),
    "DL4":point(Vector3(-0.184,-0.026,-6.728)),
    # Aleron Debout Right
    "DR1":point(Vector3( 0.184,-0.026,-4.997)),
    "DR2":point(Vector3( 0.184, 1.626,-6.553)),
    "DR3":point(Vector3( 0.184, 1.751,-7.514)),
    "DR4":point(Vector3( 0.184,-0.026,-6.728)),
    
    }

Lignes = [
    # Body
    ("BRT2","BRT1"),("BRT3","BRT2"),("BRT4","BRT3"),("BRT5","BRT4"),
    ("BRB2","BRB1"),("BRB3","BRB2"),("BRB4","BRB3"),("BRB5","BRB4"),
    ("BRT1","BRB1"),("BRT2","BRB2"),("BRT3","BRB3"),("BRT4","BRB4"),("BRT5","BRB5"),
    ("BLT2","BLT1"),("BLT3","BLT2"),("BLT4","BLT3"),("BLT5","BLT4"),
    ("BLB2","BLB1"),("BLB3","BLB2"),("BLB4","BLB3"),("BLB5","BLB4"),
    ("BLT1","BLB1"),("BLT2","BLB2"),("BLT3","BLB3"),("BLT4","BLB4"),("BLT5","BLB5"),

    ("BLT1","BRT1"),("BLT2","BRT2"),("BLT3","BRT3"),("BLT4","BRT4"),("BLT5","BRT5"),
    ("BLB1","BRB1"),("BLB2","BRB2"),("BLB3","BRB3"),("BLB4","BRB4"),("BLB5","BRB5"),
    # Ailes 
    ("ART1","ART5"),("ART2","ART1"),("ART3","ART2"),("ART4","ART3"),("ART6","ART5"),("ART7","ART6"),("ART8","ART7"),
    ("ALT1","ALT5"),("ALT2","ALT1"),("ALT3","ALT2"),("ALT4","ALT3"),("ALT6","ALT5"),("ALT7","ALT6"),("ALT8","ALT7"),
    ("ARB1","ARB5"),("ARB2","ARB1"),("ARB3","ARB2"),("ARB4","ARB3"),("ARB6","ARB5"),("ARB7","ARB6"),("ARB8","ARB7"),
    ("ALB1","ALB5"),("ALB2","ALB1"),("ALB3","ALB2"),("ALB4","ALB3"),("ALB6","ALB5"),("ALB7","ALB6"),("ALB8","ALB7"),
    # Ailerons
    ("ERT4","ERT8"),("ERT2","ERT1"),("ERT3","ERT2"),("ERT4","ERT3"),("ERT6","ERT5"),("ERT7","ERT6"),("ERT8","ERT7"),
    ("ELT4","ELT8"),("ELT2","ELT1"),("ELT3","ELT2"),("ELT4","ELT3"),("ELT6","ELT5"),("ELT7","ELT6"),("ELT8","ELT7"),
    # Aileron Debout
    ("DL1","DL2"),("DL2","DL3"),("DL3","DL4"),
    ("DR1","DR2"),("DR2","DR3"),("DR3","DR4"),
    ("DR1","DL1"),("DR2","DL2"),("DR3","DL3"),("DR4","DL4")
]

Layer3D:graphics2D.Layer3D = graphics2D.layer_new3DLayer()
planeObject = Layer3D.objects_add(Points,Lignes)
graphics2D.fill(Layer3D,(100,100,100,0))
cameraPosition = Vector3(0,0,0)
cameraAngle = Vector3(0,0,0)
Layer3D.setCameraPosition(cameraPosition)
Layer3D.render3D()
graphics2D.drawn_cercle(Layer3D,(150,150),20,noir)


def moveFront():
    print("x",planeObject.p.z)
    planeObject.p.z += 1
    planeObject.changeOrigine()
    # cameraPosition.z += 1
    # Layer3D.setCameraPosition(cameraPosition)
    Layer3D.render3D()
def moveBack():
    print("w",planeObject.p.z)
    planeObject.p.z -= 1
    planeObject.changeOrigine()
    # cameraPosition.z -= 1
    # Layer3D.setCameraPosition(cameraPosition)
    Layer3D.render3D()
def moveAngleFront():
    planeObject.setOrientation(Vector3(planeObject.o.x + 0.25*pi ,planeObject.o.y,planeObject.o.z))
    planeObject.changeOrigine()
    Layer3D.render3D()
def moveAngleBack():
    planeObject.setOrientation(Vector3(planeObject.o.x - 0.25*pi ,planeObject.o.y,planeObject.o.z))
    planeObject.changeOrigine()
    Layer3D.render3D()
def moveAngleLeft():
    planeObject.o.z += 0.25*pi
    planeObject.changeOrigine()
    Layer3D.render3D()
def moveAngleRight():
    planeObject.o.z -= 0.25*pi
    planeObject.changeOrigine()
    Layer3D.render3D()


def test():
    print("Hello World")
graphics2D.addLinkInput_down("K_z",test)
graphics2D.addLinkInput_down("K_x",moveFront)
graphics2D.addLinkInput_down("K_w",moveBack)
graphics2D.addLinkInput_down("K_z",moveAngleFront)
graphics2D.addLinkInput_down("K_s",moveAngleBack)
graphics2D.addLinkInput_down("K_d",moveAngleLeft)
graphics2D.addLinkInput_down("K_q",moveAngleRight)

mouseButtonPress = False
def printMousePosition(press):
    global mouseButtonPress
    print("  > ",press,"=>",graphics2D.getMousePosition())
    mouseButtonPress = press
    # graphics2D.setResolution((800,480))
graphics2D.addLinkMouseInput_down(1,printMousePosition,True)
graphics2D.addLinkMouseInput_up(1,printMousePosition,False)

global lastPositionMouse
lastPositionMouse = None
while graphics2D.ifNotQuit():
    positionMouse:Vector2 = graphics2D.getMousePosition()

    if positionMouse!=lastPositionMouse:
        planeObject.o.z = (positionMouse.x-resolution.x//2 )/100
        planeObject.o.x = (positionMouse.y-resolution.y//2 )/100
        # planeObject.o.z += (positionMouse.x-resolution.x//2 )/1000
        # planeObject.o.x += (positionMouse.y-resolution.y//2 )/1000
        planeObject.changeOrigine()
        Layer3D.render3D()

    if mouseButtonPress:
        graphics2D.drawn_cercle(background2,positionMouse,10,noir)
    mousePointerA.center = positionMouse
    mousePointerB.center = positionMouse
    graphics2D.render()
    graphics2D.wait(20)

    lastPositionMouse = positionMouse

graphics2D.waitQuit()



























