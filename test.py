
from math import cos, sin, radians, abs
import graphics
from vector import Vector2, Vector3


screenSize = Vector2(180*2,240*2)
worldCameraPos = Vector3(-100,-100,0)
worldCameraAngle = Vector3(0,0,0)
repeat = 20
viewFactor = 203


def setupAngle ():
    global AngleCos, AngleSin
    AngleCos = Vector3(cos(worldCameraAngle.x),cos(worldCameraAngle.y),cos(worldCameraAngle.z))
    AngleSin = Vector3(sin(worldCameraAngle.x),sin(worldCameraAngle.y),sin(worldCameraAngle.z))


def setupPoint(Point : Vector3):
    localPos = Point-worldCameraPos
    global AngleCos, AngleSin
    x = AngleCos.x * (AngleSin.z*localPos.y + AngleCos.z*x) - AngleSin.y*localPos.z
    y = AngleSin.x * (AngleCos.y*localPos.z + AngleSin.y*(AngleSin.z*localPos.y + AngleCos.z*localPos.x)) + AngleCos.x *(AngleCos.z*localPos.y - AngleSin.z*localPos.x)
    z = AngleCos.x * (AngleCos.y*localPos.z + AngleSin.y*(AngleSin.z*localPos.y + AngleCos.z*localPos.x)) - AngleSin.x *(AngleCos.z*localPos.y - AngleSin.z*localPos.x)
    return Vector3(x,y,z)

def drawLineFrom(_PointA:Vector3, _PointB:Vector3):
    pointA = setupPoint(_PointA)
    pointB = setupPoint(_PointB)
    changeBy = (pointB-pointA)/repeat
    if (0<pointA.z) and abs(pointA.y*viewFactor /pointA.z) < screenSize.y/2 and abs(pointA.x*viewFactor /pointA.z) < screenSize.x/2:
        # In s