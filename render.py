
from math import cos, sin, radians, pi
import graphics
from vector import Vector2, Vector3



screenSize = Vector2(240*4,180*3)
worldCameraPos = Vector3(-0.5,0,0)
worldCameraAngle = Vector3(0,0,0)

global distanceCameraOfPlane
distanceCameraOfPlane = 6
repeat = 20 # Nbr of cut of line
viewFactor = 200 # FOV (defaut 200)

buttonSizeDirection = 100


class point:
    def __init__(self,pos:Vector3) -> None:
        self.p = pos
        self.r = None  # Relatif
        self.c = None  # Screen Position

class Object3D:
    def __init__(self,position,orientation=Vector3(0,0,0),size=Vector3(1,1,1)) -> None:
        self.p = position    # Position in 3D world
        self.o = orientation # Relatif Orientation of Object 
        self.s = size        # Size relatif object (defaut Vector3(1,1,1) )

        self.points = {}
        self.lines = []
        self.surfaces = []
        
        self.relatifPoints = {}

        self.color_points = (0,0,0,255)
        self.color_lines = (100,100,100,255)
        self.color_surface = (200,200,200,255)
    
    def importModel(self,points,lines,surfaces):
        self.points   = points
        self.lines    = lines
        self.surfaces = surfaces

    def importColor(self,color_points,color_lines,color_surface):
        self.color_points  = color_points 
        self.color_lines   = color_lines  
        self.color_surface = color_surface
    
    def changeOrigine(self):
        LocalAngleCos = Vector3(cos(worldCameraAngle.x),cos(worldCameraAngle.y),cos(worldCameraAngle.z))
        LocalAngleSin = Vector3(sin(worldCameraAngle.x),sin(worldCameraAngle.y),sin(worldCameraAngle.z))

        self.relatifPoints = {}

        for iPointName in self.points:
            iPoint:point = self.points[iPointName]
            localPos = iPoint.p -worldCameraPos

            pos_x = LocalAngleCos.y * (LocalAngleSin.z*localPos.y + LocalAngleCos.z*localPos.x) - LocalAngleSin.y*localPos.z
            pos_y = LocalAngleSin.x * (LocalAngleCos.y*localPos.z + LocalAngleSin.y*(LocalAngleSin.z*localPos.y + LocalAngleCos.z*localPos.x)) + LocalAngleCos.x *(LocalAngleCos.z*localPos.y - LocalAngleSin.z*localPos.x)
            pos_z = LocalAngleCos.x * (LocalAngleCos.y*localPos.z + LocalAngleSin.y*(LocalAngleSin.z*localPos.y + LocalAngleCos.z*localPos.x)) - LocalAngleSin.x *(LocalAngleCos.z*localPos.y - LocalAngleSin.z*localPos.x)
            
            self.relatifPoints[iPointName] = Vector3(pos_x,pos_y,pos_z)




Points={
    # Body Right Top
    "BRT1":point(Vector3(0.651,-0.973,3.202)),
    "BRT2":point(Vector3(1,0,1.202)),
    "BRT3":point(Vector3(1,0,-1.769)),
    "BRT4":point(Vector3(0.364,0,-5.886)),
    "BRT5":point(Vector3(0.365,-0.010,-6.690)),
    # Body Right Bottom
    "BRB1":point(Vector3(0.651,-1.881,3.202,)),
    "BRB2":point(Vector3(1,-2.100,1.202)),
    "BRB3":point(Vector3(1,-2.100,-1.769)),
    "BRB4":point(Vector3(0.364,-0.884,-5.886)),
    "BRB5":point(Vector3(0.365,-0.300,-6.690)),
    
    # Body Left Top
    "BLT1":point(Vector3(-0.651,-0.973,3.202)),
    "BLT2":point(Vector3(-1,0,1.202)),
    "BLT3":point(Vector3(-1,0,-1.769)),
    "BLT4":point(Vector3(-0.364,0,-5.886)),
    "BLT5":point(Vector3(-0.365,-0.010,-6.690)),
    # Body Left Bottom
    "BLB1":point(Vector3(-0.651,-1.881,3.202,)),
    "BLB2":point(Vector3(-1,-2.100,1.202)),
    "BLB3":point(Vector3(-1,-2.100,-1.769)),
    "BLB4":point(Vector3(-0.364,-0.884,-5.886)),
    "BLB5":point(Vector3(-0.365,-0.300,-6.690)),

    # Ailes Left Top
    "ALT1":point(Vector3(-6.976,0.091,0.204)),
    "ALT2":point(Vector3(-5.333,0.091,0.764)),
    "ALT3":point(Vector3(-2.667,0.091,1)),
    "ALT4":point(Vector3(0,0.091,1)),
    "ALT5":point(Vector3(-6.976,0.091,-1.562)),
    "ALT6":point(Vector3(-5.333,0.091,-1.562)),
    "ALT7":point(Vector3(-2.667,0.091,-1.562)),
    "ALT8":point(Vector3(0,0.091,-1.562)),
    # Ailes Right Top
    "ART1":point(Vector3(6.976,0.091,0.204)),
    "ART2":point(Vector3(5.333,0.091,0.764)),
    "ART3":point(Vector3(2.667,0.091,1)),
    "ART4":point(Vector3(0,0.091,1)),
    "ART5":point(Vector3(6.976,0.091,-1.562)),
    "ART6":point(Vector3(5.333,0.091,-1.562)),
    "ART7":point(Vector3(2.667,0.091,-1.562)),
    "ART8":point(Vector3(0,0.091,-1.562)),
    # Ailes Left Bottom
    "ALB1":point(Vector3(-6.976,-0.091,0.204)),
    "ALB2":point(Vector3(-5.333,-0.091,0.764)),
    "ALB3":point(Vector3(-2.667,-0.091,1)),
    "ALB4":point(Vector3(0     ,-0.091,1)),
    "ALB5":point(Vector3(-6.976,-0.091,-1.562)),
    "ALB6":point(Vector3(-5.333,-0.091,-1.562)),
    "ALB7":point(Vector3(-2.667,-0.091,-1.562)),
    "ALB8":point(Vector3(0     ,-0.091,-1.562)),
    # Ailes Right Bottom
    "ARB1":point(Vector3(6.976,-0.091,0.204)),
    "ARB2":point(Vector3(5.333,-0.091,0.764)),
    "ARB3":point(Vector3(2.667,-0.091,1)),
    "ARB4":point(Vector3(0    ,-0.091,1)),
    "ARB5":point(Vector3(6.976,-0.091,-1.562)),
    "ARB6":point(Vector3(5.333,-0.091,-1.562)),
    "ARB7":point(Vector3(2.667,-0.091,-1.562)),
    "ARB8":point(Vector3(0    ,-0.091,-1.562)),
    
    # Aleron Left Top
    "ELT1":point(Vector3(-0  ,0.121,-5.411)),
    "ELT2":point(Vector3(-1  ,0.121,-5.411)),
    "ELT3":point(Vector3(-2.9,0.121,-5.411)),
    "ELT4":point(Vector3(-3.5,0.121,-6.411)),

    "ELT5":point(Vector3(-0  ,0.121,-6.836)),
    "ELT6":point(Vector3(-1  ,0.121,-6.836)),
    "ELT7":point(Vector3(-2.9,0.121,-6.836)),
    "ELT8":point(Vector3(-3.5,0.121,-6.836)),
    # Aleron Right Top
    "ERT1":point(Vector3(0  ,0.121,-5.411)),
    "ERT2":point(Vector3(1  ,0.121,-5.411)),
    "ERT3":point(Vector3(2.9,0.121,-5.411)),
    "ERT4":point(Vector3(3.5,0.121,-6.411)),

    "ERT5":point(Vector3(0  ,0.121,-6.836)),
    "ERT6":point(Vector3(1  ,0.121,-6.836)),
    "ERT7":point(Vector3(2.9,0.121,-6.836)),
    "ERT8":point(Vector3(3.5,0.121,-6.836)),
    
    # Aleron Debout Left
    "DL1":point(Vector3(-0.184,-0.026,-4.997)),
    "DL2":point(Vector3(-0.184, 1.626,-6.553)),
    "DL3":point(Vector3(-0.184, 1.751,-7.514)),
    "DL4":point(Vector3(-0.184,-0.026,-6.728)),
    # Aleron Debout Right
    "DR1":point(Vector3( 0.184,-0.026,-4.997)),
    "DR2":point(Vector3( 0.184, 1.626,-6.553)),
    "DR3":point(Vector3( 0.184, 1.751,-7.514)),
    "DR4":point(Vector3( 0.184,-0.026,-6.728)),
    
    }


Lignes = [
    # Body
    ("BRT2","BRT1"),("BRT3","BRT2"),("BRT4","BRT3"),("BRT5","BRT4"),
    ("BRB2","BRB1"),("BRB3","BRB2"),("BRB4","BRB3"),("BRB5","BRB4"),
    ("BRT1","BRB1"),("BRT2","BRB2"),("BRT3","BRB3"),("BRT4","BRB4"),("BRT5","BRB5"),
    ("BLT2","BLT1"),("BLT3","BLT2"),("BLT4","BLT3"),("BLT5","BLT4"),
    ("BLB2","BLB1"),("BLB3","BLB2"),("BLB4","BLB3"),("BLB5","BLB4"),
    ("BLT1","BLB1"),("BLT2","BLB2"),("BLT3","BLB3"),("BLT4","BLB4"),("BLT5","BLB5"),

    ("BLT1","BRT1"),("BLT2","BRT2"),("BLT3","BRT3"),("BLT4","BRT4"),("BLT5","BRT5"),
    ("BLB1","BRB1"),("BLB2","BRB2"),("BLB3","BRB3"),("BLB4","BRB4"),("BLB5","BRB5"),
    # Ailes 
    ("ART1","ART5"),("ART2","ART1"),("ART3","ART2"),("ART4","ART3"),("ART6","ART5"),("ART7","ART6"),("ART8","ART7"),
    ("ALT1","ALT5"),("ALT2","ALT1"),("ALT3","ALT2"),("ALT4","ALT3"),("ALT6","ALT5"),("ALT7","ALT6"),("ALT8","ALT7"),
    ("ARB1","ARB5"),("ARB2","ARB1"),("ARB3","ARB2"),("ARB4","ARB3"),("ARB6","ARB5"),("ARB7","ARB6"),("ARB8","ARB7"),
    ("ALB1","ALB5"),("ALB2","ALB1"),("ALB3","ALB2"),("ALB4","ALB3"),("ALB6","ALB5"),("ALB7","ALB6"),("ALB8","ALB7"),
    # Ailerons
    ("ERT4","ERT8"),("ERT2","ERT1"),("ERT3","ERT2"),("ERT4","ERT3"),("ERT6","ERT5"),("ERT7","ERT6"),("ERT8","ERT7"),
    ("ELT4","ELT8"),("ELT2","ELT1"),("ELT3","ELT2"),("ELT4","ELT3"),("ELT6","ELT5"),("ELT7","ELT6"),("ELT8","ELT7"),
    # Aileron Debout
    ("DL1","DL2"),("DL2","DL3"),("DL3","DL4"),
    ("DR1","DR2"),("DR2","DR3"),("DR3","DR4"),
    ("DR1","DL1"),("DR2","DL2"),("DR3","DL3"),("DR4","DL4")
]

newPlaneObject = Object3D(Vector3(0,0,0))
newPlaneObject.importModel(Points,Lignes,[])
newPlaneObject.changeOrigine()
# print(newPlaneObject.relatifPoints)

PointsCube = {
    "Rx":point(Vector3(1,0,0)),
    "Ry":point(Vector3(0,1,0)),
    "Rz":point(Vector3(0,0,1)),
    "Ro":point(Vector3(0,0,0)),

    "A":point(Vector3(1,1,1)),
    "B":point(Vector3(1,1,2)),
    "C":point(Vector3(1,2,2)),
    "D":point(Vector3(1,2,1)),
    "E":point(Vector3(0,1,1)),
    "F":point(Vector3(0,1,2)),
    "G":point(Vector3(0,2,2)),
    "H":point(Vector3(0,2,1))
    }

LignesCube = [
    ("A","B"),("B","C"),("C","D"),("D","A"),
    ("A","E"),("B","F"),("C","G"),("D","H"),
    ("E","F"),("F","G"),("G","H"),("H","E")
]


def moyenPoints():
    moy = Vector3(0,0,0)
    for iKey in Points:
        moy += Points[iKey].p
    return moy / len(Points)

positionPlane = moyenPoints()
print(positionPlane.x,positionPlane.y,positionPlane.z)


def setupAngle ():
    global AngleCos, AngleSin
    AngleCos = Vector3(cos(worldCameraAngle.x),cos(worldCameraAngle.y),cos(worldCameraAngle.z))
    AngleSin = Vector3(sin(worldCameraAngle.x),sin(worldCameraAngle.y),sin(worldCameraAngle.z))


def setupPoint(Point : Vector3):
    localPos = Point-worldCameraPos
    global AngleCos, AngleSin
    pos_x = AngleCos.y * (AngleSin.z*localPos.y + AngleCos.z*localPos.x) - AngleSin.y*localPos.z
    pos_y = AngleSin.x * (AngleCos.y*localPos.z + AngleSin.y*(AngleSin.z*localPos.y + AngleCos.z*localPos.x)) + AngleCos.x *(AngleCos.z*localPos.y - AngleSin.z*localPos.x)
    pos_z = AngleCos.x * (AngleCos.y*localPos.z + AngleSin.y*(AngleSin.z*localPos.y + AngleCos.z*localPos.x)) - AngleSin.x *(AngleCos.z*localPos.y - AngleSin.z*localPos.x)
    return Vector3(pos_x,pos_y,pos_z)

def pointPositionInView(_PointA,c):
    pointA = setupPoint(_PointA)
    # print("PointA :",pointA.x,pointA.y,pointA.z)
    # changeBy = (pointB-pointA)/repeat

    # In screen
    # e = 2*viewFactor / pointA.z
    
    if (0<pointA.z) and abs(pointA.y*viewFactor /pointA.z) < screenSize.y/2 and abs(pointA.x*viewFactor /pointA.z) < screenSize.x/2:
        if pointA.z == 0:
            pointA.z = 0.00001
        posOnScreen = Vector2(pointA.x*viewFactor / pointA.z, pointA.y*viewFactor / pointA.z) 
            
        posOnScreenCenter = posOnScreen + screenSize/2
        graphics.affiche_cercle_plein(posOnScreenCenter,abs(int(1/pointA.z*20)),c)
        return pointA,posOnScreenCenter
    else:
        # print("Out Screen")
        return None,None

    # else:
    #     # Out Screen
    #     e = 0
    #     posOnScreen = Vector2(pointA.x*viewFactor / pointA.z, pointA.y*viewFactor / pointA.z)      
    #     posOnScreenCenter = posOnScreen + screenSize/2
    #     graphics.affiche_cercle_plein(posOnScreenCenter,abs(int(1/pointA.z*10)),c)
    
def drawLineFrom(_PointA:Vector3, _PointB:Vector3,c):
    pass

class Plane:
    def __init__(self) -> None:
        self.pos = Vector3(0,0,0)        
        self.orientation = Vector2(0,0)

    def display(self):
        ##graphics.affiche_triangle_plein()
        # point_avant3D = self.pos + Vector3(0,0,10) - worldCameraPos
        # point_avant2D = Vector2(0, sin(radians(self.orientation.x)) *50)

        point_avant2D = pointPositionInView(Vector3(1,-1,1), 1)    
        point_avant2D = pointPositionInView(Vector3(0,-1,1), (225,112,112,255))    
        point_avant2D = pointPositionInView(Vector3(0,-1,-1), (112,255,112,255)) 
        point_avant2D = pointPositionInView(Vector3(1,-1,-1), (112,112,255,255))   
        point_avant2D = pointPositionInView(Vector3(0,-1,0), (112,255,112,255)) 
        point_avant2D = pointPositionInView(Vector3(1,-1,0), (112,112,255,255)) 

        # point_avant2D = drawLineFrom(Vector3(1,-1,1),Vector3(60,40,20) , 1)    
        # point_avant2D = drawLineFrom(Vector3(0,-1,1),Vector3(60,40,20), (225,112,112,255))    
        # point_avant2D = drawLineFrom(Vector3(0,-1,-1),Vector3(60,40,20), (112,255,112,255)) 
        # point_avant2D = drawLineFrom(Vector3(1,-1,-1),Vector3(60,40,20), (112,112,255,255)) 







plane = Plane()





def CreateWindow():
    graphics.init_fenetre(screenSize[0], screenSize[1], "Game : Paper Plane")
    graphics.affiche_auto_off()


def displayALigne(positionA,positionB,couleur,epaiseur):
    graphics.affiche_ligne(positionA,positionB,couleur,epaiseur)

def calculePositionAllPoints():
    for iKey in Points:
        element = Points[iKey]
        element.r, element.c = pointPositionInView(element.p,(255,100,100,255))
    

def displayAllLignes():
    for element in Lignes:
        pA,pB = Points[element[0]].c,Points[element[1]].c
        if pA and pB:
            e = int((Points[element[0]].r.z + Points[element[1]].r.z)*0.5) 
            if e ==0:
                e = 0.0001
            displayALigne(pA,pB, (50,255,50,255), abs(int(1/e*20)))



def Render():
    graphics.remplir_fenetre((255,255,255,255))
    setupAngle()
    
    calculePositionAllPoints()
    displayAllLignes()

    # plane.display()
    graphics.affiche_tout()


CreateWindow()
Render()
# graphics.attendre_echap()


def CameraMoveA():
    clic = Vector2(graphics.wait_clic())
    if clic.x <buttonSizeDirection:
        if clic.y <buttonSizeDirection: # Corner Bottom Left
            worldCameraAngle.y -= 0.25*pi
        elif clic.y >screenSize.y-buttonSizeDirection: # Corner Top Left
            worldCameraPos.y -= 1
        else:
            worldCameraPos.x -= 1

    elif clic.x >screenSize.x-buttonSizeDirection: 
        if clic.y <buttonSizeDirection: # Corner Bottom Right
            worldCameraAngle.y += 0.25*pi
        elif clic.y >screenSize.y-buttonSizeDirection: # Corner Top Right
            worldCameraPos.y += 1
        else:
            worldCameraPos.x += 1
    else:
        if clic.y <buttonSizeDirection:
            worldCameraPos.z -= 1
        elif clic.y >screenSize.y-buttonSizeDirection:
            worldCameraPos.z += 1
        else:
            pass

def CameraMoveB():
    clic = Vector2(graphics.wait_clic())
    for i in range(10):
        global distanceCameraOfPlane
        if clic.x <buttonSizeDirection: # Corner Bottom Left
            if clic.y <buttonSizeDirection: # Corner Bottom Right
                distanceCameraOfPlane -= 0.1
            else:
                worldCameraAngle.y -= 0.025*pi
        elif clic.x >screenSize.x-buttonSizeDirection: # Corner Top Left
            if clic.y <buttonSizeDirection: # Corner Bottom Right
                distanceCameraOfPlane += 0.1
            else:
                worldCameraAngle.y += 0.025*pi
        else:
            pass

        d = distanceCameraOfPlane
        worldCameraPos.x = 0-cos(0-worldCameraAngle.y+0.5*pi)*d + positionPlane.x
        worldCameraPos.y = d #sin(worldCameraAngle.y)*d + positionPlane.x
        worldCameraPos.z = 0-sin(0-worldCameraAngle.y+0.5*pi)*d + positionPlane.z

        worldCameraAngle.x = pi*0.25

        # cameraObjectifPos = Vector2()
        # cameraObjectifPos.x = 0-sin(0-worldCameraAngle.y)*d + positionPlane.x
        # cameraObjectifPos.z = 0-cos(0-worldCameraAngle.y)*d + positionPlane.z

        # pointATest = Vector2(positionPlane.x,positionPlane.z) 
        # pointBTest = Vector2(worldCameraPos.x,worldCameraPos.z)
        
        # graphics.remplir_fenetre((255,255,255,255))
        Render()

        # graphics.affiche_texte(str(worldCameraAngle.y),Vector2(5,5),2,20)


        # graphics.affiche_cercle_plein((pointATest*2+ screenSize//2).floor(),4,(0,0,0,255))
        # graphics.affiche_cercle_plein((pointBTest*2+ screenSize//2).floor(),4,(50,0,0,255))
        # # graphics.affiche_cercle_plein((10,10),20,2)
        # graphics.affiche_tout()
        # print("Centre:",pointATest,"Cam:",pointBTest)


while graphics.pas_echap():

    CameraMoveB()