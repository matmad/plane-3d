
from graphics2D import *
from vector import Vector3
from math import cos,sin,tan, pi
import numpy as np

__worldCameraPos = Vector3(0,0,0)
__worldCameraAngle = Vector3(0,0,0)

repeat = 20 # Nbr of cut of line
viewFactor = 200 # FOV (defaut 200)




class point:
    def __init__(self,pos:Vector3) -> None:
        self.p = pos
        self.r = None  # Relatif
        self.c = None  # Screen Position

class Object3D:
    def __init__(self,position,orientation=Vector3(0,0,0),size=Vector3(1,1,1)) -> None:
        self.p:Vector3 = position    # Position in 3D world
        self.o:Vector3 = orientation # Relatif Orientation of Object 
        self.s:Vector3 = size        # Size relatif object (defaut Vector3(1,1,1) )
        self.pivot = Vector3(0,0,0)

        self.points = {}
        self.lines = []
        self.surfaces = []
        
        self.relatifPoints = {}

        self.color_points = (0,0,0,255)
        self.color_lines = (100,100,100,255)
        self.color_surface = (200,200,200,255)

        self.parent:Layer3D = None
    
    def importModel(self,points,lines,surfaces):
        self.points   = points
        self.lines    = lines
        self.surfaces = surfaces

    def importColor(self,color_points,color_lines,color_surface):
        self.color_points  = color_points 
        self.color_lines   = color_lines  
        self.color_surface = color_surface
    
    def setPosition(self,newPosition):
        self.p = newPosition
    
    def setOrientation(self,newOrientation):
        self.o = newOrientation
    
    def setPivotPoint(self,newPivotPoint):
        self.pivot = newPivotPoint

    def changeOrigine(self):
        # worldCameraPos= self.parent.getCameraPosition()
        # worldCameraAngle = self.parent.getCameraAngle()
        LocalAngleCos = Vector3(cos(self.o.x),cos(self.o.y),cos(self.o.z))
        LocalAngleSin = Vector3(sin(self.o.x),sin(self.o.y),sin(self.o.z))

        self.relatifPoints = {}

        print(self.s.x)
        for iPointName in self.points:
            iPoint:point = self.points[iPointName]
            localPos = iPoint.p * self.s.x #+self.p 
            if iPointName == "1":
                print(localPos)

            pos_x = self.p.x + LocalAngleCos.y * (LocalAngleSin.z*localPos.y + LocalAngleCos.z*localPos.x) - LocalAngleSin.y*localPos.z
            pos_y = self.p.y + LocalAngleSin.x * (LocalAngleCos.y*localPos.z + LocalAngleSin.y*(LocalAngleSin.z*localPos.y + LocalAngleCos.z*localPos.x)) + LocalAngleCos.x *(LocalAngleCos.z*localPos.y - LocalAngleSin.z*localPos.x)
            pos_z = self.p.z + LocalAngleCos.x * (LocalAngleCos.y*localPos.z + LocalAngleSin.y*(LocalAngleSin.z*localPos.y + LocalAngleCos.z*localPos.x)) - LocalAngleSin.x *(LocalAngleCos.z*localPos.y - LocalAngleSin.z*localPos.x)
            
            self.relatifPoints[iPointName] = Vector3(pos_x,pos_y,pos_z)


# --- Init --- #
class Layer3D(Layer):
    def __init__(self, size) -> None:
        print(size)
        super().__init__(size)
        self.workspace:list[Object3D] = list()  # List of objest in layer
        self.worldCameraPos = Vector3(0,0,0)    # Position Local of Camera
        self.worldCameraAngle = Vector3(0,0,0)  # Orientation Local of Camera
        self.cameraMove = False                 # If the position or angle of camera change enter two render

    def objects_add_Object3D(self,newObject3D:Object3D):
        """ To add a new 3d object to the layer/scene """
        newObject3D.parent = self
        newObject3D.changeOrigine()
        self.workspace.append(newObject3D)
        return newObject3D
    
    def objects_add(self,points,lines=[],surfaces=[]):
        """ To add a new 3d object to the layer/scene """
        newObject = Object3D(Vector3(0,0,0))
        newObject.importModel(points,lines,surfaces)
        
        newObject.parent = self
        newObject.changeOrigine()

        self.workspace.append(newObject)
        return newObject

    # --- Render of Layer --- #
    def render3D(self):
        """ Render of Layer : create a surface(image) 
            /!\ Warn : don't display to screen        """
        AngleCos, AngleSin = camera_setupAngle(self.worldCameraAngle)
        self.surface_clear()

        for iObject in self.workspace:
            # if self.cameraMove:
            iObject.changeOrigine()
            dictPointsRelatifObject = iObject.relatifPoints
            dictPointsRelatifCamera = transform_RelatifObject_To_RelatifCamera(self.worldCameraPos,AngleCos, AngleSin,dictPointsRelatifObject)
            dictPointsOnView = transform_RelatifCamera_To_RelatifScreen(self,dictPointsRelatifCamera)
            displayAllLignes(self,dictPointsRelatifCamera, dictPointsOnView, iObject.lines)
        self.cameraMove = False

    # --- Camera --- #
    def setCameraPosition(self,newPosition:Vector3):
        self.worldCameraPos = newPosition
        self.cameraMove = True
    def getCameraPosition(self):
        return self.worldCameraPos
    def setCameraAngle(self,newAngle:Vector3):
        self.worldCameraAngle = newAngle
        self.cameraMove = True
    def getCameraAngle(self):
        return self.worldCameraAngle

layers3D:list[Layer3D] = list()


def layer_new3DLayer() -> Layer3D:
    global layers3D
    newLayer3D = Layer3D(getResolution())
    layers3D.append(newLayer3D)
    layer_add(newLayer3D)
    return newLayer3D





# --- Simple 3D Object --- #
class Object3D_Cube(Object3D):
    def __init__(self, position, orientation=Vector3(0, 0, 0), size=Vector3(1, 1, 1)) -> None:
        super().__init__(position, orientation, size)
        self.importModel(
            {
            "1":point(Vector3(-1,-1,-1)),
            "2":point(Vector3( 1,-1,-1)),
            "3":point(Vector3(-1, 1,-1)),
            "4":point(Vector3( 1, 1,-1)),
            "5":point(Vector3(-1,-1, 1)),
            "6":point(Vector3( 1,-1, 1)),
            "7":point(Vector3(-1, 1, 1)),
            "8":point(Vector3( 1, 1, 1))},[
            ("1","2"),("5","6"),("1","5"),
            ("2","4"),("6","8"),("2","6"),
            ("4","3"),("8","7"),("3","7"),
            ("3","1"),("7","5"),("4","8")],
            []
            )


class Object3D_Grid(Object3D):
    def __init__(self, position, orientation=Vector3(0, 0, 0), size=Vector3(1, 1, 1), gridSize=Vector2(10,10), heigt=1) -> None:
        super().__init__(position, orientation, size)
        self.gs = gridSize
        self.h = heigt
        
        pointDict = {}
        linesList = list()
        surfaceList = list()

        for iz in range(gridSize.y):
            for ix in range(gridSize.x):
                namePoint = self.getNamePoint(ix,iz)
                pointDict[namePoint] = point(Vector3(ix,0,iz))
                if ix<gridSize.x-1:
                    linesList.append((namePoint,self.getNamePoint(ix+1,iz)))
                    if iz<gridSize.y-1:
                        linesList.append((namePoint,self.getNamePoint(ix,iz+1)))
                        linesList.append((namePoint,self.getNamePoint(ix+1,iz+1)))
                else:
                    if iz<gridSize.y-1:
                        linesList.append((namePoint,self.getNamePoint(ix,iz+1)))

        self.importModel(pointDict,linesList,surfaceList)

    def getNamePoint(self,ix,iz):
        return str(ix)+"_"+str(iz)
    
    def load_HeighMap(self,heighMap):
        for iz in range(self.gs.y):
            for ix in range(self.gs.x):
                namePoint = self.getNamePoint(ix,iz)
                self.points[namePoint] = point(Vector3(ix,heighMap[iz][ix]*self.h,iz))







# --- Transfomation of Plans --- #
def camera_setupAngle(cameraAngle):
    AngleCos = Vector3(cos(cameraAngle.x),cos(cameraAngle.y),cos(cameraAngle.z))
    AngleSin = Vector3(sin(cameraAngle.x),sin(cameraAngle.y),sin(cameraAngle.z))
    return AngleCos, AngleSin

def transform_RelatifObject_To_RelatifCamera_OnePoint(cameraPosition,AngleCos, AngleSin, Point : Vector3):
    """ Changement Plan : RelatifObject -> RelatifCamera """
    localPos:Vector3 = Point-cameraPosition
    pos_x = AngleCos.y * (AngleSin.z*localPos.y + AngleCos.z*localPos.x) - AngleSin.y*localPos.z
    pos_y = AngleSin.x * (AngleCos.y*localPos.z + AngleSin.y*(AngleSin.z*localPos.y + AngleCos.z*localPos.x)) + AngleCos.x *(AngleCos.z*localPos.y - AngleSin.z*localPos.x)
    pos_z = AngleCos.x * (AngleCos.y*localPos.z + AngleSin.y*(AngleSin.z*localPos.y + AngleCos.z*localPos.x)) - AngleSin.x *(AngleCos.z*localPos.y - AngleSin.z*localPos.x)
    return Vector3(pos_x,pos_y,pos_z)

def transform_RelatifObject_To_RelatifCamera(cameraPosition,AngleCos, AngleSin, dictPoints:dict[Vector3]):
    """ Changement Plan : RelatifObject -> RelatifCamera """
    newRelatifPositionOfCamera = {}
    for iKey in dictPoints:
        pointRelatifObject = dictPoints[iKey]
        pointRelatifCamera = transform_RelatifObject_To_RelatifCamera_OnePoint(cameraPosition,AngleCos, AngleSin,pointRelatifObject)
        newRelatifPositionOfCamera[iKey] = pointRelatifCamera
    return newRelatifPositionOfCamera


def transform_RelatifCamera_To_RelatifScreen_OnePoint(layer3D:Layer3D,pointA,c):
    """ Changement Plan : RelatifCamera (Vector3) -> RelatifScreen (Vector2) """
    resolutionLayer3D = layer3D.size
    if (0<pointA.z) and abs(pointA.y*viewFactor /pointA.z) < resolutionLayer3D.y/2 and abs(pointA.x*viewFactor /pointA.z) < resolutionLayer3D.x/2:
        if pointA.z == 0:
            pointA.z = 0.00001
        posOnScreen = Vector2(pointA.x*viewFactor / pointA.z, pointA.y*viewFactor / pointA.z) 
            
        posOnScreenCenter = posOnScreen + resolutionLayer3D/2
        posOnScreenCenter.y = layer3D.size.y-posOnScreenCenter.y  # posWithAxeYInversed
        drawn_cercle(layer3D,posOnScreenCenter,abs(int(1/pointA.z*20)),c)
        return posOnScreenCenter
    else:
        # print("Out Screen")
        return None

def transform_RelatifCamera_To_RelatifScreen(layer3D:Layer3D,dictPointRelatifCamera:dict[Vector3]):
    """ Changement Plan : RelatifCamera (Vector3) -> RelatifScreen (Vector2) """
    dictPointRelatifScreen:dict[Vector2] = {}
    for iKey in dictPointRelatifCamera:
        element:Vector3 = dictPointRelatifCamera[iKey]
        newPositionOnScreen = transform_RelatifCamera_To_RelatifScreen_OnePoint(layer3D,element,(255,100,100,255))
        dictPointRelatifScreen[iKey] = newPositionOnScreen
    return dictPointRelatifScreen


def displayAllLignes(layer3D:Layer3D,listPointsRelatif:dict[Vector3],listPointsScreen:dict[Vector2],listLines:list):
    for element in listLines:
        pA,pB = listPointsScreen[element[0]],listPointsScreen[element[1]]
        if pA and pB:
            e = int((listPointsRelatif[element[0]].z + listPointsRelatif[element[1]].z)*0.5) 
            if e ==0:
                e = 0.0001
            drawn_line(layer3D,pA,pB, (50,255,50,255), abs(int(1/e*20)))


# --- Functions Callable --- #
def moyenPoints(points:list[point]):
    moy = Vector3(0,0,0)
    for iKey in points:
        moy += points[iKey].p
    return moy / len(points)


# --- Camera --- #


# --- Functions Render --- #
def render3D():
    for iLayer in layers3D:
        iLayer.render3D()