
import graphics2D
from vector import Vector2
from random import randint

__resolution = Vector2(720,720)
__sizeTetris = Vector2(10,20)


__color_background = (0,0,0,255)
__color_lines = (255,255,255,255)

__colorPiece = [0,(255,0,0,255),(0,255,0,255),(0,0,255,255)]
__colorPieceNbr = len(__colorPiece)

# ---- # Data # --- #
def data_case_newCase(color):
    return color

def data_newGame_newTable(size):
    return [[None for ix in range(size.x)] for iy in range(size.y)]

def data_newGame(size):
    newTable = data_newGame_newTable(size)
    newGame = {"size":size,"score":0,"space":newTable}
    return newGame

def data_getSpace(game):
    return game["space"]
def data_getSize(game) -> Vector2:
    return game["size"]

def data_modify_ifPosInTable(size:Vector2,pos:Vector2):
    return (pos.x>=0 and pos.y>=0 and pos.x<size.x and pos.y<size.y)

def data_case_modify(game,pos:Vector2,val):
    size = data_getSize(game)
    table = data_getSpace(game)
    if data_modify_ifPosInTable(size,pos):
        table[pos.y][pos.x] = val

def data_case_get(game,pos:Vector2):
    size = data_getSize(game)
    table = data_getSpace(game)
    if data_modify_ifPosInTable(size,pos):
        return table[pos.y][pos.x]

def data_getCopyTable(game):
    table = data_getSpace(game)
    size  = data_getSize(game)
    return [[table[iy][ix] for ix in range(size.x)] for iy in range(size.y)]

def data_table_deletCompletedLines_IfLineColmpleted(line):
    for i in line:
        if not(i):
            return False
    return True

def data_table_deletCompletedLines_downLine(table,startLine):
    sizeY = len(table)
    # for i in range(len(table[startLine])):
    #     table[startLine][i] = None
    for iy in range(startLine,1,-1):
        # print("Line Down",iy)
        if iy>0:
            table[iy] = table[iy-1]
        else:
            print("Warn : so stange")

def data_table_deletCompletedLines(table):
    nbrLineDestroy = 0
    for iy,line in enumerate(table):
        if data_table_deletCompletedLines_IfLineColmpleted(line):
            nbrLineDestroy += 1
            data_table_deletCompletedLines_downLine(table,iy)
    return nbrLineDestroy 

def data_table_countHole(table):
    holeCount = 0
    for iy,line in enumerate(table):
        for ix,case in enumerate(line):
            if not(case) and table[iy-1][ix]:
                holeCount +=1
    return holeCount

def data_game_newPiece_newTable(size:Vector2):
    return [[None for ix in range(size.x)] for iy in range(size.y)]

def data_game_newPiece(table):
    # newTable = data_game_newPiece_newTable()
    color = randint(1,__colorPieceNbr-1)
    size = Vector2(len(table[0]),len(table)) 
    return {"t":table,"c":color,"s":size}

def data_piece_getColor(piece):
    return piece["c"]
def data_piece_getShape(piece):
    return piece["t"]
def data_piece_getSize(piece):
    return piece["s"]

def data_piece_setColor(piece,newColor):
    piece["c"] = newColor
def data_piece_setShape(piece,newTable):
    piece["t"] = newTable
def data_piece_setSize(piece,newSize):
    piece["s"] = newSize

def data_piece_getCopy(piece):
    return {"t":piece["t"],"c":piece["c"],"s":piece["s"]}

def data_piece_rotation_left(piece):
    pieceShape = data_piece_getShape(piece)
    pieceSize  = data_piece_getSize(piece)
    newSize  = Vector2(pieceSize.y,pieceSize.x)
    newTable = data_game_newPiece_newTable(newSize)

    for iy,line in enumerate(pieceShape):
        for ix,case in enumerate(line):
            newTable[ix][pieceSize.y-iy-1] = case

    data_piece_setShape(piece,newTable)
    data_piece_setSize(piece,newSize)


def data_game_selecteARandowPiece():
    listPiece = [
        # data_game_newPiece([
        #     [True]
        # ]),
        data_game_newPiece([
            [True,True]
        ]),
        data_game_newPiece([
            [True,True,True]
        ]),
        # data_game_newPiece([
        #     [True,True,True,True]
        # ]),
        data_game_newPiece([
            [True,True,True,True,True]
        ]),
        data_game_newPiece([
            [True,True,True],
            [False,True,False]
        ]),
        data_game_newPiece([
            [True,True,True],
            [False,False,True]
        ]),
        data_game_newPiece([
            [True,True,True],
            [True,False,False]
        ])
        # data_game_newPiece([
        #     [False,True,False],
        #     [True,True,True],
        #     [False,True,False]
        # ]),
        # data_game_newPiece([
        #     [True,True],
        #     [True,True]
        # ])
        # data_game_newPiece([
        #     [False,False,False],
        #     [False,False,False],
        #     [False,False,False]
        # ]),
    ]
    return listPiece[randint(0,len(listPiece)-1)]

def data_piece_testColision_ifCollistion(game,piece,position):
    gameTable = data_getSpace(game)
    gameSize = data_getSize(game)
    caseColorID = data_piece_getColor(piece)
    caseShape = data_piece_getShape(piece)
    for iy,line in enumerate(caseShape):
        for ix,case in enumerate(line):
            positionCase = position+Vector2(ix,iy)
            if data_modify_ifPosInTable(gameSize,positionCase):
                if case and gameTable[positionCase.y][positionCase.x]:
                    return True
            else:
                return True
    return False


def data_piece_testColision_canFall(game,piece,position):
    return not(data_piece_testColision_ifCollistion(game,piece,position+Vector2(0,1)))

def data_piece_anchoredPiece_Game(game,piece,position):
    caseColorID = data_piece_getColor(piece)
    caseShape = data_piece_getShape(piece)
    for iy,line in enumerate(caseShape):
        for ix,case in enumerate(line):
            if case:
                positionCase = position+Vector2(ix,iy)
                data_case_modify(game,positionCase,caseColorID)

def data_piece_anchoredPiece_table(table,size,piece,position):
    caseColorID = data_piece_getColor(piece)
    caseShape = data_piece_getShape(piece)
    for iy,line in enumerate(caseShape):
        for ix,case in enumerate(line):
            if case:
                positionCase = position+Vector2(ix,iy)
                if data_modify_ifPosInTable(size,positionCase):
                    table[positionCase.y][positionCase.x] = caseColorID


__game = data_newGame(__sizeTetris)

















# ---- # Affichage # --- #
__grillPosition = Vector2(10,10)
__grillSize = Vector2(10,10)

graphics2D.newWindow(__resolution)
# graphics2D.setWindowResizable()

layer_background:graphics2D.Layer = graphics2D.newLayer()
layer_background.surface_fill((0,0,0,255))
layer_main:graphics2D.Layer = graphics2D.newLayer()
# layer_main.fill(__color_background)
graphics2D.drawn_rect(layer_main,Vector2(50,50),Vector2(50,50),(0,0,0,255))

def update_sizeScreen():
    global __grillSize,__sizeCase
    __grillSize = Vector2(0,__resolution.y-20) 
    __grillSize.x = int(__grillSize.y/__sizeTetris.y * __sizeTetris.x)
    graphics2D.drawn_rect_empty(layer_background, __grillPosition  ,__grillSize,__color_lines,6)
    graphics2D.drawn_rect_empty(layer_background, __grillPosition+2,__grillSize-4,__color_background,2)

    sizeTruc = Vector2(__resolution.x-20-10-__grillSize.x , 200)
    graphics2D.drawn_rect_empty(layer_background, Vector2(__grillSize.x+20,150),sizeTruc,__color_lines,6)
    graphics2D.drawn_rect_empty(layer_background, Vector2(__grillSize.x+22,152),sizeTruc-4,__color_background,2)

    __sizeCase = __grillSize.y/__sizeTetris.y
update_sizeScreen()


def drawn_case(pos,colorId):
    sizeCase = __sizeCase
    sizeCaseVec = Vector2(sizeCase,sizeCase)
    posPixel:Vector2 = (__grillPosition + pos*sizeCase).floor()
    graphics2D.drawn_rect(layer_main,posPixel,Vector2(sizeCase,sizeCase),__colorPiece[colorId])
    graphics2D.drawn_rect_empty(layer_main,posPixel,Vector2(sizeCase,sizeCase),__color_lines,1)

    miniCubePos:Vector2 = posPixel+sizeCase//6
    miniCubeSize:Vector2 = Vector2(sizeCase//6 *4,sizeCase//6 *4)
    points = [posPixel,posPixel+sizeCaseVec.OnlyX(),posPixel+sizeCase,posPixel+sizeCaseVec.OnlyY(), miniCubePos,miniCubePos+miniCubeSize.OnlyX(),miniCubePos+miniCubeSize,miniCubePos+miniCubeSize.OnlyY()]
    r,g,b,a = __colorPiece[colorId]
    colorsPoligon = [(r,g,b,200),(r,g,b,150),(r,g,b,100),(r,g,b,120)]
    for i in range(4):
        graphics2D.drawn_polygon(layer_main,[points[i],points[(i+1)%4],points[(i+1)%4+4],points[i+4]],colorsPoligon[i])

def drawn_piece(pos,piece):
    caseColorID = data_piece_getColor(piece)
    caseShape = data_piece_getShape(piece)
    for iy,line in enumerate(caseShape):
        for ix,case in enumerate(line):
            if case:
                drawn_case(pos+Vector2(ix,iy),caseColorID)

def drawn_table(table):
    for iy,line in enumerate(table):
        for ix,case in enumerate(line):
            if case:
                drawn_case(Vector2(ix,iy),case)










# --- # Game # --- #
__pieceFall = None
__piecePosition:Vector2 = Vector2(0,0)

def move_Left(piece, vectorPosition):
    if not(data_piece_testColision_ifCollistion(__game,piece,vectorPosition+Vector2(-1,0))):
        # vectorPosition+=Vector2(-1,0)
        vectorPosition.x -= 1
        return True
def move_Right(piece,vectorPosition):
    if not(data_piece_testColision_ifCollistion(__game,piece,vectorPosition+Vector2(1,0))):
        # vectorPosition+=Vector2(1,0)
        vectorPosition.x += 1
        return True

def move_Rotation_Left(piece,vectorPosition):
    lastSize = data_piece_getSize(piece)
    lastShape = data_piece_getShape(piece)[:]
    lastPosition = vectorPosition
    data_piece_rotation_left(piece)
    newSize = data_piece_getSize(piece)
    vectorPosition = vectorPosition+lastSize//2-newSize//2

    if data_piece_testColision_ifCollistion(__game,piece,vectorPosition):
        # print("Imposible Rotation",lastSize,newSize)
        data_piece_setSize(piece,lastSize)
        # print(data_piece_getSize(piece))
        data_piece_setShape(piece,lastShape)
        vectorPosition = lastPosition
        return False
    return True

def input_move_left():
    global __piecePosition
    return move_Left(__pieceFall,__piecePosition)
def input_move_right():
    global __piecePosition
    return move_Right(__pieceFall,__piecePosition)
def input_move_rotationLeft():
    global __piecePosition
    return move_Rotation_Left(__pieceFall,__piecePosition)

graphics2D.addLinkInput_down("K_q",input_move_left)
graphics2D.addLinkInput_down("K_d",input_move_right)
graphics2D.addLinkInput_down("K_a",input_move_rotationLeft)
# graphics2D.addLinkInput_down("K_d",move_Right)











# ---- # Computer IA # --- #
def computer_evalutionChose(table):
    score = 0
    nbr_case = 0
    data_table_deletCompletedLines(table)
    
    # holeCount = data_table_countHole(table)
    # score += holeCount*5
    
    for iy,line in enumerate(table):
        for ix,case in enumerate(line):
            if case:
                nbr_case = 1
                level = __sizeTetris.y-iy-1
                score += nbr_case + level
    return score

def computer_testOnePossibility(piece,centerX,iMoveRight,iMoveRotation):
    positionPieceCopy = Vector2(centerX,3)

    for iMoveRight_Rotation in range(iMoveRotation):
        if not(move_Rotation_Left(piece,positionPieceCopy)):
            return
        
    if iMoveRight>0:
        for iMoveRight_Repeate in range(iMoveRight):
            if not(move_Right(piece,positionPieceCopy)):
                return
    elif iMoveRight<0:
        for iMoveRight_Repeate in range(abs(iMoveRight)):
            if not(move_Left(piece,positionPieceCopy)):
                return
        
    copyTable = data_getCopyTable(__game)

    while data_piece_testColision_canFall(__game,piece,positionPieceCopy):
        positionPieceCopy.y +=1

    data_piece_anchoredPiece_table(copyTable,__sizeTetris,piece,positionPieceCopy)
    score = computer_evalutionChose(copyTable)

    # render(piece,positionPieceCopy)
    # graphics2D.wait(10)
    return score


def computer_displayAllPossibility():
    global __piecePosition

    centerX = 3

    mainScore = None
    bestMove_iMoveRight = 0
    bestMove_iMoveRotation = 0
    
    print("----- New Test -----")
    for iMoveRight in range(-centerX,__sizeTetris.x-centerX):
        for iMoveRotation in range(0,4):
            pieceCopy = data_piece_getCopy(__pieceFall)
            score = computer_testOnePossibility(pieceCopy,centerX,iMoveRight,iMoveRotation)
            if (mainScore==None and score) or (score and score < mainScore):
                mainScore = score
                bestMove_iMoveRight = iMoveRight
                bestMove_iMoveRotation = iMoveRotation
                print("New Best Move :",score,iMoveRight,iMoveRotation)
    
    __piecePosition = Vector2(0,3)

    for i in range(centerX):
        input_move_right()
    for iMoveRight_Rotation in range(bestMove_iMoveRotation):
        graphics2D.wait(100)
        render(__pieceFall,__piecePosition)
        if not(input_move_rotationLeft()):
            print("Error A")
    if bestMove_iMoveRight>0:
        for iMoveRight_Repeate in range(bestMove_iMoveRight):
            graphics2D.wait(100)
            render(__pieceFall,__piecePosition)
            if not(input_move_right()):
                print("Error B")
    elif bestMove_iMoveRight<0:
        for iMoveRight_Repeate in range(abs(bestMove_iMoveRight)):
            graphics2D.wait(100)
            render(__pieceFall,__piecePosition)
            if not(input_move_left()):
                print("Error C")

graphics2D.addLinkInput_down("K_z",computer_displayAllPossibility)









# tableOriginal = data_getSpace(__game)
# tableCopy = data_getCopyTable(__game)
# tableOriginal[0][0] = -5
# print(tableOriginal[0][0],tableCopy[0][0])

# graphics2D.wait(2000)




# ---- # Main Loop # --- #
def render(pieceFall=__pieceFall,pieceFallPosition=__piecePosition):
    layer_main.surface_fill((0,0,0,0))
    drawn_table(data_getSpace(__game))
    drawn_piece(pieceFallPosition,pieceFall)
    graphics2D.render()

iFrame = 0
while graphics2D.ifNotQuit():
    iFrame += 1

    if iFrame >=1:
        iFrame =0
        if __pieceFall:
            if data_piece_testColision_canFall(__game,__pieceFall,__piecePosition):
                __piecePosition.y +=1
            else:
                data_piece_anchoredPiece_Game(__game,__pieceFall,__piecePosition)
                __pieceFall = None
                print(" > Evalution:",computer_evalutionChose(data_getCopyTable(__game) ))
                data_table_deletCompletedLines(data_getSpace(__game))

        layer_main.surface_fill((0,0,0,0))
        if not(__pieceFall):
            __pieceFall = data_game_selecteARandowPiece()
            # print(__pieceFall)
            __piecePosition = Vector2(0,0)
            computer_displayAllPossibility()

        drawn_piece(__piecePosition,__pieceFall)
        drawn_table(data_getSpace(__game))

        graphics2D.render()
    graphics2D.wait(100)














