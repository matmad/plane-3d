

# --- # Imports # --- #
import graphics2D
from graphics2D import UDim2
from vector import Vector2
from random import randint
from math import atan,cos,sin, sqrt

# --- # Constante # --- #
__Resolution = Vector2(480,360)*2
__timePerFrame = 20

__boidSize = 4

__colorBackground = (0,0,0,255)
__colorBoids = (255,0,0,255)

frameCalculate = 0
menuVisible = False

# --- # Parameters Variable # --- #
TOPSPEED = 2.85 # Target Speed  (TopSpeed=[0.5,5.0])
RESOLVE = 0.2 # (Resolve = [0.01,1.00])
RANGE = 75 # Max distance of the boids can see (Range = [20,150])
SEPARATION = 0.2 # Tue amount of force this Boid will excerpt to keep it away from any Boid within site (Separation = [0.05,0.5])
COHENSION = 0.03 # Group the bird in RANGE (COHENSION = [0.0,0.1])
ALIGNMENT = 0.02 # Alignment the any birds in RANGE (COHENSION = [0.0,0.1])
INFLUENCE = 0.05 # Influence the color   (INFLUENCE = [0.0,1.0])
NUMBER = 200     # Number of birds in scene   (NUMBER = [0,400])



# --- # Setup Window # --- #
graphics2D.setResolution(__Resolution)
graphics2D.setTitle("Boids Simulator Project")
mainLayer = graphics2D.newLayer()
mainLayer.surface_fill(__colorBackground)



# --- # Setup Menu # --- #
__color_fillButton = (0,0,0,0)
__color_outlineButton = (255,255,255,255)
guiLayer:graphics2D.GuiLayer = graphics2D.layer_newGuiLayer()

def button_start():
    print("Start")

def buttonFunc_showMenu():
    # menuVisible = not(menuVisible)
    frame.visible = not(frame.visible)
    button_showMenu.text = "<" if frame.visible else ">"

# guiLayer.objects_add(graphics2D.VecRect(Vector2(6,6),Vector2(100,20),__color_outlineButton,__color_fillButton,2),button_start)
button_showMenu = guiLayer.objects_add(graphics2D.VecText_Framed(">",Vector2(6,6),Vector2(30,30),__color_outlineButton,__color_outlineButton,__color_fillButton,2,24),buttonFunc_showMenu)
frame:graphics2D.Gui_Frame = guiLayer.objects_add(graphics2D.Gui_Frame(Vector2(6,40),Vector2(160,400),__color_outlineButton,__color_fillButton,2),None)
# frame.objects_add(graphics2D.VecRect(Vector2(6,6),Vector2(100-12,20),__color_outlineButton,__color_fillButton,2),button_start)
# frame.objects_add(graphics2D.VecRect(UDim2(0,6,0,36),UDim2(1,-12,0,30),__color_outlineButton,__color_fillButton,2),button_start)
frame.objects_add(graphics2D.VecText_Framed("Mains :",UDim2(0,0,0,0),UDim2(1,0,0,30),__color_outlineButton,__color_outlineButton,__color_fillButton,1,20),button_start)
frame.objects_add(graphics2D.VecText_Framed("Boids :",UDim2(0,0,0,118),UDim2(1,0,0,30),__color_outlineButton,__color_outlineButton,__color_fillButton,1,20),button_start)
# frame.objects_add(graphics2D.Gui_Silder(UDim2(0,6,0,40),UDim2(1,-12,0,30),__color_outlineButton,__color_fillButton))


def parameters_set_TOPSPEED(): 
    global TOPSPEED  
    TOPSPEED = silder_TopSpeed.value
silder_TopSpeed = frame.objects_add(graphics2D.Gui_Silder_WithText("Top Speed : ",UDim2(0,6,0,34),UDim2(1,-12,0,40),__color_outlineButton,__color_fillButton),parameters_set_TOPSPEED)
silder_TopSpeed.setExtrmumSilder(0.5,8)
silder_TopSpeed.setValueTo(TOPSPEED)

def parameters_set_NUMBER(): 
    global NUMBER  
    NUMBER = int(silder_Number.value)
    numberActual = len(__boidsList)
    dif = NUMBER - numberActual
    if dif>0:
        for i in range(dif):
            __boidsList.append(Boid(numberActual+i+1))
    elif dif<0:
        for i in range(-dif):
            __boidsList.pop(numberActual-i-1)


silder_Number = frame.objects_add(graphics2D.Gui_Silder_WithText("Number : ",UDim2(0,6,0,74),UDim2(1,-12,0,40),__color_outlineButton,__color_fillButton),parameters_set_NUMBER)
silder_Number.setExtrmumSilder(1,400)
silder_Number.setValueTo(NUMBER)







# --- # Boids # --- #
def getRandomPosition ():
    return Vector2(randint(0,__Resolution.x),randint(0,__Resolution.y))
def getRandomSpeedPosition(offset):
    return Vector2(randint(-offset,offset),randint(-offset,offset))

def distanceP2(pA:Vector2,pB:Vector2):
    return (pA.x-pB.x)**2 + (pA.y-pB.y)**2

def newColor(_ref=None):
    ref = _ref if _ref else frameCalculate
    iColor = (ref*2)%(255*3)
    if iColor<=255:
        return (iColor,0,255-iColor,255)
    elif iColor<=255*2:
        return (255-(iColor%255),iColor%255,0,255)
    elif iColor<=255*3:
        return (0,255-(iColor%255),iColor%255,255)
    return (255,255,255,255)

class Boid:
    def __init__(self,id) -> None:
        self.p:Vector2 = getRandomPosition()
        self.sp:Vector2 = getRandomSpeedPosition(5)
        self.o = 0
        self.id = id # number of element in list
        self.c = newColor(int((self.p.x+self.p.y)/4))
    def directionUpdate(self):
        self.o = atan(self.sp.y/self.sp.x)
    def calculate(self,listOthersBoids:list, mousePos:Vector2):
        negativeSeparation = -SEPARATION
        boidCount = 0
        sumBoids = Vector2(0,0)
        sumSpeedBoids = Vector2(0,0)
        sumColor = [0,0,0] #[self.c[0],self.c[1],self.c[2]]
        for i, otherBoid in enumerate(listOthersBoids):
            if i != self.id:
                distanceX = otherBoid.p.x - self.p.x 
                distanceY = otherBoid.p.y - self.p.y 
                distance = sqrt(distanceX**2+distanceY**2)
                if distance < RANGE and distance!=0:
                    boidCount += 1
                    sumBoids.x += distanceX
                    sumBoids.y += distanceY

                    sumSpeedBoids.x += otherBoid.sp.x - self.sp.x
                    sumSpeedBoids.y += otherBoid.sp.y - self.sp.y

                    self.sp.x += negativeSeparation * (distanceX/distance)
                    self.sp.y += negativeSeparation * (distanceY/distance)

                    sumColor[0] += otherBoid.c[0]
                    sumColor[1] += otherBoid.c[1]
                    sumColor[2] += otherBoid.c[2]

        if boidCount != 0:
            self.sp.x += COHENSION * (sumBoids.x / boidCount)
            self.sp.y += COHENSION * (sumBoids.y / boidCount)
            self.sp.x += ALIGNMENT * (sumSpeedBoids.x / boidCount)
            self.sp.y += ALIGNMENT * (sumSpeedBoids.y / boidCount)

            # self.c = [
            #     int((sumColor[0] + self.c[0])/(boidCount+1) +0.5),
            #     int((sumColor[1] + self.c[1])/(boidCount+1) +0.5),
            #     int((sumColor[2] + self.c[2])/(boidCount+1) +0.5),
            #     255]
            self.c = [
                int(self.c[0] + (sumColor[0]/boidCount - self.c[0])*INFLUENCE +0.5), 
                int(self.c[1] + (sumColor[1]/boidCount - self.c[1])*INFLUENCE +0.5),
                int(self.c[2] + (sumColor[2]/boidCount - self.c[2])*INFLUENCE +0.5),
                255]
            
            self.c[0] = self.c[0] if self.c[0] <= 255 else 255
            self.c[1] = self.c[1] if self.c[1] <= 255 else 255
            self.c[2] = self.c[2] if self.c[2] <= 255 else 255

            self.c = tuple(self.c)
        
        distancePointerX = mousePos.x - self.p.x 
        distancePointerY = mousePos.y - self.p.y 
        distanceToPointer = sqrt(distancePointerX**2 + distancePointerY**2)
        if distanceToPointer < RANGE:
            self.sp.x += negativeSeparation * (distancePointerX/distanceToPointer)
            self.sp.y += negativeSeparation * (distancePointerY/distanceToPointer)


    def move(self,__Resolution):
        distanceAuCarre = distanceP2(self.p, self.p+self.sp)
        distance = sqrt(distanceAuCarre)

        if distance != 0:
            target = (self.sp.x/distance) * TOPSPEED
            self.sp.x += RESOLVE * (target - self.sp.x)
            target = (self.sp.y/distance) * TOPSPEED
            self.sp.y += RESOLVE * (target - self.sp.y)

            self.p += self.sp

            if self.p.x <0:
                self.p.x += __Resolution.x
                self.c = newColor()
            if self.p.x >__Resolution.x:
                self.p.x -= __Resolution.x
                self.c = newColor()
            if self.p.y <0:
                self.p.y += __Resolution.y
                self.c = newColor()
            if self.p.y >__Resolution.y:
                self.p.y -= __Resolution.y
                self.c = newColor()

__boidsList:list[Boid] = list()

def createXBoids(nbrX):
    for i in range(nbrX):
        __boidsList.append(Boid(i))

def drawn_boid(boid:Boid):
    # pA = Vector2( boid.p.x + cos()*__boidSize , )
    # graphics2D.drawn_triangle(mainLayer)
    # print(boid.c)
    graphics2D.drawn_line(mainLayer,boid.p, boid.p+(boid.sp*5), boid.c, __boidSize)
    graphics2D.drawn_cercle(mainLayer,boid.p,4,boid.c)



# --- # Input # --- #
def TopSpeed_add(offset):
    global TOPSPEED
    TOPSPEED += offset
    print("TOPSPEED =",TOPSPEED)
graphics2D.addLinkInput_down("K_z",TopSpeed_add,0.25)
graphics2D.addLinkInput_down("K_s",TopSpeed_add,-0.25)

def RandomParameters():
    global TOPSPEED,RESOLVE,RANGE,SEPARATION,COHENSION,ALIGNMENT 
    # TOPSPEED   = randint(5,50)*0.1   # Target Speed  (TopSpeed=[0.5,5.0])
    RESOLVE    = randint(1,100)*0.01 # (Resolve = [0.01,1.00])
    RANGE      = randint(20,150)     # Max distance of the boids can see (Range = [20,150])
    SEPARATION = randint(5,50)*0.1   # The amount of force this Boid will excerpt to keep it away from any Boid within site (Separation = [0.05,0.5])
    COHENSION  = randint(0,1)*0.1    # Group the bird in RANGE (COHENSION = [0.0,0.1])
    ALIGNMENT  = randint(0,1)*0.1    # Alignment the any birds in RANGE (COHENSION = [0.0,0.1])
# graphics2D.addLinkMouseInput_down(1,RandomParameters)


def parameters_set_RESOLVE(): 
    global RESOLVE  
    RESOLVE = int(silder_Resolve.value*100)*0.01
silder_Resolve = frame.objects_add(graphics2D.Gui_Silder_WithText("Resolve : ",UDim2(0,6,0,150),UDim2(1,-12,0,40),__color_outlineButton,__color_fillButton),parameters_set_RESOLVE)
silder_Resolve.setExtrmumSilder(0.01,1.00)
silder_Resolve.setValueTo(RESOLVE)

def parameters_set_RANGE(): 
    global RANGE  
    RANGE = int(silder_Range.value)
silder_Range = frame.objects_add(graphics2D.Gui_Silder_WithText("Range : ",UDim2(0,6,0,190),UDim2(1,-12,0,40),__color_outlineButton,__color_fillButton),parameters_set_RANGE)
silder_Range.setExtrmumSilder(20,150)
silder_Range.setValueTo(RANGE)

def parameters_set_SEPARATION(): 
    global SEPARATION  
    SEPARATION = silder_Separation.value
silder_Separation = frame.objects_add(graphics2D.Gui_Silder_WithText("Separation : ",UDim2(0,6,0,230),UDim2(1,-12,0,40),__color_outlineButton,__color_fillButton),parameters_set_SEPARATION)
silder_Separation.setExtrmumSilder(0.0,1.0)
silder_Separation.setValueTo(SEPARATION)

def parameters_set_COHENSION(): 
    global COHENSION  
    COHENSION = silder_Cohesion.value
silder_Cohesion = frame.objects_add(graphics2D.Gui_Silder_WithText("Cohesion : ",UDim2(0,6,0,270),UDim2(1,-12,0,40),__color_outlineButton,__color_fillButton),parameters_set_COHENSION)
silder_Cohesion.setExtrmumSilder(0.0,0.1)
silder_Cohesion.setValueTo(COHENSION)

def parameters_set_ALIGNMENT(): 
    global ALIGNMENT  
    ALIGNMENT = silder_Alignment.value
silder_Alignment = frame.objects_add(graphics2D.Gui_Silder_WithText("Alignment : ",UDim2(0,6,0,310),UDim2(1,-12,0,40),__color_outlineButton,__color_fillButton),parameters_set_ALIGNMENT)
silder_Alignment.setExtrmumSilder(0.0,0.1)
silder_Alignment.setValueTo(ALIGNMENT)

def parameters_set_INFLUENCE(): 
    global INFLUENCE  
    INFLUENCE = silder_Influence.value
silder_Influence = frame.objects_add(graphics2D.Gui_Silder_WithText("Influence : ",UDim2(0,6,0,350),UDim2(1,-12,0,40),__color_outlineButton,__color_fillButton),parameters_set_INFLUENCE)
silder_Influence.setExtrmumSilder(0.0,1.0)
silder_Influence.setValueTo(INFLUENCE)



def anyBirdRandomPosition():
    for iBoid in __boidsList:
        iBoid.p = getRandomPosition()
graphics2D.addLinkMouseInput_down(3,anyBirdRandomPosition)


graphics2D.addLinkInput_down("K_SPACE",createXBoids,NUMBER)
graphics2D.addLinkInput_down("K_q",createXBoids,NUMBER)



# --- # Render # --- #
def renderBoids():
    mousePosition = graphics2D.getMousePosition()
    mainLayer.surface_fill(__colorBackground)
    for i, boid in enumerate(__boidsList):
        boid.calculate(__boidsList,mousePosition)
        boid.move(__Resolution)
        drawn_boid(boid)



# --- # Main Loop # --- #
while graphics2D.ifNotQuit():
    frameCalculate += 1
    renderBoids()
    if len(__boidsList) == 0:
        graphics2D.drawn_textCenter(mainLayer,"Warn Epileptic !",__Resolution//2,(255,255,255,255),30)
        graphics2D.drawn_textCenter(mainLayer,"(Press Space)",__Resolution//2-Vector2(0,-30),(255,255,255,255),20)

    graphics2D.render()
    graphics2D.wait(__timePerFrame)




















