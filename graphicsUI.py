
from graphics2D import *




# --- Gui Objects --- #
class Button:
    def __init__(self,functionDrawn=None,functionTest=None,functionCall=None,functionParameters=None) -> None:
        self.functionTest = functionTest
        self.functionCall = functionCall
        self.functionDrawn= functionDrawn
        self.functionParameters = functionParameters

class Button_Rect(Button):
    def __init__(self, functionDrawn=None, functionTest=None, functionCall=None, functionParameters=None) -> None:
        super().__init__(functionDrawn, functionTest, functionCall, functionParameters)
        BasicVectoriel.__init__(self,3,4,5)
    def drawn(self,layer):
        if self.fillColor:
            drawn_rect(layer,self.pos,self.size,self.fillColor)
        if self.thickness != 0 and self.color:
            drawn_rect_empty(layer,self.pos,self.size,self.color,self.thickness)


def detection_rect(button:Button,mousePos:Vector2):
    return mousePos.x>= button.pos.x and mousePos.y>= button.pos.y and mousePos.x<button.pos.x+button.size.x and mousePos.y<button.pos.y+button.size.y
def detection_rectangle(button:Button,mousePos:Vector2):
    return mousePos.x>= button.posA.x and mousePos.y>= button.posA.y and mousePos.x<button.posB.x and mousePos.y<button.posB.y
def detection_circular(button:Button,mousePos:Vector2):
    return (button.center.x-mousePos.x)**2 + (button.center.y-mousePos.y)**2 <= button.rayon




# class Button_Rect(VecRect):
#     def __init__(self, pos, size, color, fillColor, thickness) -> None:
#         super().__init__(pos, size, color, fillColor, thickness)
    
#     def inButton(self,pos:Vector2):
#         return pos.x>= self.pos.x and pos.y>= self.pos.y and pos.x<self.pos.x+self.size.x and pos.y<self.pos.y+self.size.y









