"""Exemple d'affichage d'une fenêtre simple."""
import pygame
from pygame.locals import K_RETURN, K_SPACE, KEYDOWN, KEYUP, QUIT, RESIZABLE


pygame.init()
__screen = None

class Window():
    def __init__(self,resolution) -> None:
        self.s = pygame.display.set_mode(resolution,RESIZABLE)
        self.background = pygame.Surface(self.s.get_size())
        self.background = self.background.convert()

    def setCaption(self,title):
        pygame.display.set_caption(title)   

    def fillScreen(self,color = (250, 250, 250)):
        """ Fill background with a color"""
        self.background.fill(color)

    def display_Text(self, ):
        """ Display some text """
        font = pygame.font.Font(None, 36)
        text = font.render("Hello There", 1, (10, 10, 10))
        textpos = text.get_rect()
        textpos.centerx = self.background.get_rect().centerx
        self.background.blit(text, textpos)

    def render(self):
        """Blit everything to the screen"""
        self.s.blit(self.background, (0, 0))
        pygame.display.flip()






def newWindow(resolution):
    """ Initialise a new screen """
    newScreen = Window(resolution)
    newScreen.setCaption("New Window")
    __screen = newScreen
    return newScreen


screen = newWindow((600,400))
screen.fillScreen()
screen.display_Text()

# Event loop
continuer = True
while continuer:
    for event in pygame.event.get():
        if event.type == QUIT:
            continuer = False
        elif event.type == KEYDOWN:
            if event.key == K_SPACE:
                print("Space Down")
            elif event.key == K_RETURN:
                print("Return Down")
        elif event.type == KEYUP:
            if event.key == K_SPACE:
                print("Space Up")
    screen.render()















